
<?php get_header();?>

<!-- Page Header -->
<!-- Set your background image for this header on the line below. -->
<header class="intro-header" style="background-image: url('<?php bloginfo('template_directory');?>/img/home-bg.jpg')">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                <div class="site-heading">
                    <h1><?php bloginfo('name');?></h1>
                    <hr class="small">
                    <span class="subheading"><?php bloginfo('description');?></span>
                </div>
            </div>
        </div>
    </div>
</header>

<!-- Main Content -->
<div class="container">
    <div class="row">
        <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
            <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                <div class="post-preview">
                    <a href="<?php the_permalink(); ?>" rel="bookmark">
                        <h2 class="post-title">
                            <?php the_title(); ?>
                        </h2>
                    </a>
                    <p class="post-subtitle">
                        <?php the_excerpt();?>
                    </p>
                    <p class="post-meta">Escrito por <a href="#"><?php the_author();?></a> em <?php the_date();?> | <?php the_category(', '); ?></p>
                </div>
                <hr>
            <?php endwhile; ?>
                <ul class="pager">
                    <li class="next"><?php next_posts_link( 'Próxima &rarr;' ); ?></li>
                    <li class="previous"><?php previous_posts_link( ' &larr; Anterior' ); ?></li>
                </ul>
            <?php else : ?>
                <p><?php _e('Desculpe, mas não há posts com estes critérios.', 'cleanblog-wp');?></p>
            <?php endif; ?>
        </div>
    </div>
</div>
<?php get_footer();?>