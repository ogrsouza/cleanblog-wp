
    <?php get_header();?>

    <!-- Page Header -->
    <!-- Set your background image for this header on the line below. -->
    <header class="intro-header" style="background-image: url('<?php bloginfo('template_directory');?>/img/about-bg.jpg')">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                    <div class="post-heading">
                        <h1><?php the_title();?></h1>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <!-- Main Content -->
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                
                    <div class="post-preview">
                        <?php the_content();?>
                    </div>
                    <hr>
                <?php endwhile; ?>
                    <ul class="pager">
                        <li class="next"><?php next_posts_link( 'Próxima &rarr;' ); ?></li>
                        <li class="previous"><?php previous_posts_link( ' &larr; Anterior' ); ?></li>
                    </ul>
                <?php else : ?>
                    <p><?php _e('Desculpe, mas não há posts com estes critérios.');?></p>
                <?php endif; ?>
            </div>
        </div>
    </div>

<?php get_footer();?>