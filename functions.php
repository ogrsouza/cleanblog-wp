<?php

add_theme_support( 'post-thumbnails' );
/* Theme setup */
    add_action( 'after_setup_theme', 'wpt_setup' );
        if ( ! function_exists( 'wpt_setup' ) ):
            function wpt_setup() {
                register_nav_menu( 'primary', 'Primary Navigation' );
            } endif;


// Excerpt More
function new_excerpt_more( $more ) {
    return ' <a class="read-more" href="' . get_permalink( get_the_ID() ) . '">' . __( 'Continuar lendo &raquo;', 'cleanblog-wp' ) . '</a>';
}
add_filter( 'excerpt_more', 'new_excerpt_more' );


    // Class for using bootstrap menu
    class BS3_Walker_Nav_Menu extends Walker_Nav_Menu {
        /**
         * Traverse elements to create list from elements.
         *
         * Display one element if the element doesn't have any children otherwise,
         * display the element and its children. Will only traverse up to the max
         * depth and no ignore elements under that depth. It is possible to set the
         * max depth to include all depths, see walk() method.
         *
         * This method shouldn't be called directly, use the walk() method instead.
         *
         * @since 2.5.0
         *
         * @param object $element Data object
         * @param array $children_elements List of elements to continue traversing.
         * @param int $max_depth Max depth to traverse.
         * @param int $depth Depth of current element.
         * @param array $args
         * @param string $output Passed by reference. Used to append additional content.
         * @return null Null on failure with no changes to parameters.
         */
        function display_element( $element, &$children_elements, $max_depth, $depth, $args, &$output ) {
            $id_field = $this->db_fields['id'];

            if ( isset( $args[0] ) && is_object( $args[0] ) )
            {
                $args[0]->has_children = ! empty( $children_elements[$element->$id_field] );

            }

            return parent::display_element( $element, $children_elements, $max_depth, $depth, $args, $output );
        }

        /**
         * @see Walker::start_el()
         * @since 3.0.0
         *
         * @param string $output Passed by reference. Used to append additional content.
         * @param object $item Menu item data object.
         * @param int $depth Depth of menu item. Used for padding.
         * @param int $current_page Menu item ID.
         * @param object $args
         */
        function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
            if ( is_object($args) && !empty($args->has_children) )
            {
                $link_after = $args->link_after;
                $args->link_after = ' <b class="caret"></b>';
            }

            parent::start_el($output, $item, $depth, $args, $id);

            if ( is_object($args) && !empty($args->has_children) )
                $args->link_after = $link_after;
        }

        /**
         * @see Walker::start_lvl()
         * @since 3.0.0
         *
         * @param string $output Passed by reference. Used to append additional content.
         * @param int $depth Depth of page. Used for padding.
         */
        function start_lvl( &$output, $depth = 0, $args = array() ) {
            $indent = str_repeat("\t", $depth);
            $output .= "\n$indent<ul class=\"dropdown-menu list-unstyled\">\n";
        }
    }
    add_filter('nav_menu_link_attributes', function($atts, $item, $args) {
        if ( $args->has_children )
        {
            $atts['data-toggle'] = 'dropdown';
            $atts['class'] = 'dropdown-toggle';
        }

        return $atts;
    }, 10, 3);

add_action('admin_menu', 'cb_create_menu');

function cb_create_menu(){
    // Add Top level menu
    add_menu_page('Theme Settings', 'Clean Blog Settings', 'administrator', __FILE__, 'cb_theme_settings_page');

    add_action('admin_init', 'cb_register_settings');
}

function cb_register_settings(){
    register_setting('cb_settings_group', 'cb_option_facebook');
    register_setting('cb_settings_group', 'cb_option_github');
    register_setting('cb_settings_group', 'cb_option_googleplus');
}


function cb_theme_settings_page(){
    ?>
    <div class="wrap">
        <h2><?php _e('Clean Blog Theme Settings', 'cleanblog-wp');?></h2>
        <h3><?php _e('Here you can change the url\'s for the social icons on the footer of the theme.','cleanblog-wp');?></h3>
        <form method="post" action="options.php">
            <?php settings_fields('cb_settings_group'); ?>
            <table class="form-table">
                <tr valign="top">
                    <th scope="row">Facebook URL: </th>
                    <td><input type="text" name="cb_option_facebook" value="<?php echo get_option('cb_option_facebook'); ?>"/> </td>
                </tr>
                <tr valign="top">
                    <th scope="row">Github URL: </th>
                    <td><input type="text" name="cb_option_github" value="<?php echo get_option('cb_option_github'); ?>"/> </td>
                </tr>
                <tr valign="top">
                    <th scope="row">Google Plus URL: </th>
                    <td><input type="text" name="cb_option_googleplus" value="<?php echo get_option('cb_option_googleplus'); ?>"/> </td>
                </tr>
            </table>
            <p class="submit">
                <input type="submit" value="Salvar mudanças" class="button-primary">
            </p>
        </form>
    </div>



<?php
}







